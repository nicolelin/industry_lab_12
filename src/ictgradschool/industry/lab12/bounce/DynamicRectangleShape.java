package ictgradschool.industry.lab12.bounce;

import java.awt.*;

/**
 * Created by ylin183 on 18/05/2017.
 */
public class DynamicRectangleShape extends RectangleShape {

    private static final Color DEFAULT_COLOR = Color.black;

    private boolean filled = false;

    private Color color;

    public DynamicRectangleShape() {
        super();
        this.color = DEFAULT_COLOR;
    }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY) {
        this(x, y, deltaX, deltaY, DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_COLOR);
    }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        this(x, y, deltaX, deltaY, width, height, DEFAULT_COLOR);
    }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height, Color color) {
        super(x, y, deltaX, deltaY, width, height);
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public void move(int width, int height) {
        int prevDx = fDeltaX;
        int prevDy = fDeltaY;

        super.move(width,height);

        if(prevDx != fDeltaX) {
            filled = true;
        } else if (prevDy != fDeltaY) {
            filled = false;
        }

    }

    @Override
    public void paint(Painter painter) {

        if (filled) {
            Color temp = painter.getColor();

            painter.setColor(this.color);
            painter.fillRect(fX,fY,fWidth,fHeight);

            painter.setColor(temp);
        }

        super.paint(painter);
    }

}
