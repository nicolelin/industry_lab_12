package ictgradschool.industry.lab12.bounce;

import java.awt.*;

/**
 * Created by ylin183 on 18/05/2017.
 */
public class GemShape extends Shape {

    public GemShape() {
        super();
    }

    public GemShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public GemShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }

    @Override
    public void paint(Painter painter) {

        /* GEM SHAPE

        The GemShape is to be painted to fit within the given width and height.

        The definition of the coordinates of the points is phrased here in terms of (x, y, width, height):

The top-left and bottom-left vertices of a GemShape are normally 20 pixels to the right of the left-hand side of the shape.

Similarly, the top-right and bottom-right vertices of a GemShape are normally 20 pixels to the left of the right hand side of the shape.

However, if the width of a Gemshape is less than 40 pixels, the top-left and top-right vertices are both positioned at point (x + width/2, y).

Similarly, the bottom-left and bottom-right vertices are both positioned at point (x + width/2, y + height). In other words, “small” GemShapes are four-sided figures.
         */

        Polygon polygon = new Polygon();

        if (getWidth() <= 40) {
            polygon.addPoint(fX, fY + fHeight / 2);
            polygon.addPoint(fX + fWidth / 2, fY);
            polygon.addPoint(fX + fWidth, fY + fHeight / 2);
            polygon.addPoint(fX + fWidth / 2, fY + fHeight);

        }

        else {
            polygon.addPoint(fX, fY + fHeight / 2);
            polygon.addPoint(fX + 20, fY);
            polygon.addPoint(fX + fWidth - 20, fY);
            polygon.addPoint(fX + fWidth, fY + fHeight / 2);
            polygon.addPoint(fX + fWidth - 20, fY + fHeight);
            polygon.addPoint(fX + 20, fY + fHeight);
        }

        painter.drawPolygon(polygon);
    }
}
